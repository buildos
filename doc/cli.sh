#! /usr/bin/env bash

version=0.18.0-a.0.z

trap 'exit 1' ERR
set -o errtrace # Trap in functions.

function info () { echo "$*" 1>&2; }
function error () { info "$*"; exit 1; }

date="$(date +"%B %Y")"
copyright="$(sed -n -re 's%^Copyright \(c\) (.+)\.$%\1%p' ../LICENSE | head -n 1)"

while [ $# -gt 0 ]; do
  case $1 in
    --clean)
      rm -f build2-buildos-manual.xhtml
      rm -f *.ps *.pdf
      exit 0
      ;;
    *)
      error "unexpected $1"
      ;;
  esac
done

# Manuals.
#

function xhtml_to_ps () # <from> <to> [<html2ps-options>]
{
  local from="$1"
  shift
  local to="$1"
  shift

  sed -e 's/├/|/g' -e 's/│/|/g' -e 's/─/-/g' -e 's/└/\xb7/g' "$from" | \
  html2ps "${@}" -o "$to"
}

function compile_doc () # <file> <prefix> <suffix>
{
  cli -I .. \
-v version="$(echo "$version" | sed -e 's/^\([^.]*\.[^.]*\).*/\1/')" \
-v date="$date" \
-v copyright="$copyright" \
--generate-html --html-suffix .xhtml \
--html-prologue-file doc-prologue.xhtml \
--html-epilogue-file doc-epilogue.xhtml \
--output-prefix "$2" \
--output-suffix "$3" \
"$1"

  local n="$2$(basename -s .cli $1)$3"

  xhtml_to_ps "$n.xhtml" "$n-a4.ps" -f doc.html2ps:a4.html2ps
  ps2pdf14 -sPAPERSIZE=a4 -dOptimize=true -dEmbedAllFonts=true "$n-a4.ps" "$n-a4.pdf"

  xhtml_to_ps "$n.xhtml" "$n-letter.ps" -f doc.html2ps:letter.html2ps
  ps2pdf14 -sPAPERSIZE=letter -dOptimize=true -dEmbedAllFonts=true "$n-letter.ps" "$n-letter.pdf"
}

compile_doc manual.cli 'build2-buildos-'
