#! /usr/bin/env bash

# Upload the Build OS images to a TFTP server.
#
# If the tftp server host is not specified, then build@build-cache is
# assumed. The images are uploaded to /var/lib/tftpboot/buildos-devel/.
#
usage="usage: $0 [-a <arch>] [<user>@<host>]"

owd="$(pwd)"
trap "{ cd '$owd'; exit 1; }" ERR
set -o errtrace # Trap in functions.

function info () { echo "$*" 1>&2; }
function error () { info "$*"; exit 1; }

arch=

while [ "$#" -gt 0 ]; do
  case "$1" in
    -a)
      shift
      arch="$1"
      shift
      break
      ;;
    *)
      break
      ;;
  esac
done

if [ -z "$arch" ]; then
  arch="$(uname -m)"
fi

if [ -z "$1" ]; then
  host="build@build-cache"
else
  host="$1"
fi

# Use --delay-updates to make things a bit more atomic (we don't want to
# start rebooting before kernel/initrd are finished syncing). The cost
# is a bit more disk space used to temporarily hold copies.
#
rsync -v --progress -lpt -c --copy-unsafe-links --delay-updates \
  "buildos-image-$arch" "buildos-initrd-$arch" "buildos-buildid-$arch" \
  $host:/var/lib/tftpboot/buildos-devel/
