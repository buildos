# file      : buildfile
# license   : TBC; see accompanying LICENSE file

define sh: file
sh{*}: extension =

./: {*/ -build/}                                                          \
    sh{bootstrap buildos genmacaddr init qemu-ifup} file{buildos.service} \
    doc{INSTALL NEWS README} legal{LICENSE}                               \
    manifest

# Don't install INSTALL file. Scripts and buildos.service are installed by the
# bootstrap script.
#
doc{INSTALL}@./: install = false
